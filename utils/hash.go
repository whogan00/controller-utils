package utils

import (
	"fmt"
	"hash/fnv"

	"github.com/davecgh/go-spew/spew"
)

// SetHashAnnotation sets the computed hash as an annotation value and returns annotations that can be directly applied to meta
func SetHashAnnotation(annotation string, annotations map[string]string, object interface{}) map[string]string {
	if annotations == nil {
		annotations = map[string]string{}
	}
	annotations[annotation] = HashObject(object)
	return annotations
}

// HashObject of provided interface for tracking if there are changes
func HashObject(object interface{}) string {
	hf := fnv.New32()
	printer := spew.ConfigState{
		Indent:         " ",
		SortKeys:       true,
		DisableMethods: true,
		SpewKeys:       true,
	}
	_, _ = printer.Fprintf(hf, "%#v", object)
	return fmt.Sprint(hf.Sum32())
}
