package utils

import (
	apierrs "k8s.io/apimachinery/pkg/api/errors"
)

// IgnoreNotFound check if error is only for IsNotFound
func IgnoreNotFound(err error) error {
	if apierrs.IsNotFound(err) {
		return nil
	}
	return err
}
